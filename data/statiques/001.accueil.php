<div class="grid colorgrey">
   <div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

   <div class="col sml-11 med-11 lrg-4 flex450">
      <div class="encart">
         <h1>Mini Site, késako ?</h1>

         <p>Mini Site, c'est un service de création de sites internet proposé par l'association <a href="https://www.defis.info">Défis</a> à destination des petites structures et des indépendants.</p>

         <p>En quelques clics, nous réalisons une vitrine de vos activités qui sera évolutive, sécurisée et respectueuse de votre budget.</p>
         <a class="button block contact" href="index.php?contact">Contactez-nous</a>
      </div>
   </div>

   <div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

   <div class="col sml-hide med-hide lrg-6 lrg-show"><img alt="Image de présentation laptop" class="img600" src="data/medias/accueil_600-450.svg" role="img" /></div>
</div>

<div class="grid containerfxd">
<div class="col sml-11 med-11 lrg-4"><img alt="Image mains offrant de la terre avec une pousse" class="img250" src="data/medias/accueil_250-180-1.svg" role="img" />
<h1>Respecte l'environnement</h1>

<p>Conçu en respectant les normes W3C avec un CMS qui par sa légéreté consomme peu d'énergie. De plus, Défis fait le choix de les autohéberger pour un impact environnemental plus faible</p>
</div>

<div class="col sml-11 med-11 lrg-4"><img alt="Image etude d'un site responsive" class="img250" src="data/medias/accueil_250-180-2.svg" role="img"/>
<h1>Responsive Design</h1>

<p>Nous concevons vos sites de manière à ce qu'ils offrent une expérience de lecture et de navigation optimale quelle que soit son type de support (ordinateur, smartphone, tablette...).</p>
</div>

<div class="col sml-11 med-11 lrg-4"><img alt="Image représentant la sécurité d'un site" class="img250" src="data/medias/accueil_250-180-3.svg" role="img" />
<h1>Sécurisé</h1>

<p>Négliger la sécurisation de son site web ne pourra avoir qu’un impact négatif, c'est pourquoi nous nous efforçons d'optimiser et sécuriser votre site pour votre plus grand confort.</p>
</div>
</div>
