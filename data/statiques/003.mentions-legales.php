<div class="grid colorwhite">
<div class="col sml-hide med-1 med-show lrg-1 lrg-show"></div>
<div class="col sml-hide med-10 med-show lrg-10 lrg-show">
<h2>Éditeur</h2>

<h3>Défis</h3>

<ul>
<li>Association loi 1901</li>
<li>N° Siret : 50190376900021</li>
</ul>

<h3>Siège social</h3>

<ul>
<li>Association Défis<br />8 rue du Général Leclerc<br />56600 LANESTER<br />FRANCE<br />Tél : 02.97.76.34.91</li>
</ul>

<h3>Direction de la publication</h3>

<ul>
<li>Mickaël Leblond</li>
</ul>

<h3>Hébergeur</h3>

<ul>
<li>Association Défis<br />8 rue du Général Leclerc<br />56600 LANESTER<br />FRANCE</li>
</ul>

<h2>Informatique et libertés</h2>

<ul>
<li>En France, les données personnelles sont notamment protégées par la loi n° 78-17 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l’article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.<br />En tout état de cause Défis ne collecte des informations personnelles relatives à l’utilisateur (nom, adresse électronique, coordonnées téléphoniques) que pour le besoin des <a href="https://madata.defis.info/index.html#services">services proposés</a>.<br />L’utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu’il procède par lui-même à leur saisie. Il est alors précisé à l’utilisateur des sites de Défis le caractère obligatoire ou non des informations qu’il serait amené à fournir.</li>
<li>L'association Défis ne transmet pas, ne revend pas, ne divulgue pas les informations personnelles de ses utilisateurs à quelque tiers que ce soit selon les dispositions prévues par la loi.</li>
<li>Les seules personnes habilitées à consulter les fichiers adhérent sont les membres du bureau de l'association.</li>
<li>Les seules personnes habilitées à consulter les journaux de connexion sont les responsables informatique désignés au sein du conseil d'administration.</li>
<li>Dans le cadre de l'utilisation du site, des cookies peuvent être stockés sur le navigateur de l'utilisateur.</li>
</ul>

<h3>Cookies</h3>

<ul>
<li>Afin de nous permettre d'identifier un utilisateur connecté, nous stockons un <strong>cookie pour chaque utilisateur qui se connecte</strong> sur notre plateforme. Ce cookie expire à la fin de la session de navigation de l'utilisateur.</li>
<li>Les sites du réseau Défis ne stocke aucun cookie de tierce partie dans le navigateur de ses utilisateurs.</li>
<li>Les sites du réseau Défis ne stocke aucun cookie à des fins publicitaires, de pistage ou d'analyse.</li>
</ul>

<h3>Rectification des informations nominatives collectées</h3>

<ul>
<li>Conformément aux dispositions de l’article 34 de la loi n° 48-87 du 6 janvier 1978, l’utilisateur dispose d’un droit de modification des données nominatives collectées le concernant.<br />Pour ce faire, l’utilisateur envoie à l'association Défis :
<ul>
<li>un courrier électronique en utilisant le formulaire de contact</li>
<li>un courrier à l’adresse du siège de l’association (indiquée ci-dessus) en indiquant son nom ou sa raison sociale, ses coordonnées physiques et/ou électroniques, ainsi que le cas échéant la référence dont il disposerait en tant qu’utilisateur des sites du réseaux de Défis.</li>
</ul>
</li>
<li>La modification interviendra dans des délais raisonnables à compter de la réception de la demande de l’utilisateur.</li>
</ul>

<h3>Propriété intellectuelle</h3>

<ul>
<li>Les contenus sont publiés sous la responsabilité des utilisateurs.</li>
<li>Sauf mention contraire sur les supports énoncés, les contenus appartenant à l'association Défis sont sous licence <strong><a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International</a></strong>.</li>
</ul>

<h3>Limitation de responsabilité</h3>

<ul>
<li>L'association Défis ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l’utilisateur lors de l’accès aux sites du réseaux et aux services de l'association Défis, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications techniques requises, soit de l’apparition d’un bug ou d’une incompatibilité.</li>
<li>L'association Défis ne pourra également être tenue responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site.</li>
<li>Ce site comporte des informations mises à disposition par des communautés ou sociétés externes ou des liens hypertextes vers d’autres sites qui n’ont pas été développés par l'association Défis. Le contenu mis à disposition sur le site est fourni à titre informatif. L’existence d’un lien de ce site vers un autre site ne constitue pas une validation de ce site ou de son contenu. La responsabilité de l'association Défis ne saurait être engagée du fait des informations, opinions et recommandations formulées par des tiers.</li>
<li>L'association Défis ne peut être tenue responsable du contenu envoyé par ses utilisateurs et se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, l'association Défis se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé.</li>
</ul>

<h3>Limitations contractuelles sur les données techniques</h3>

<ul>
<li>L'association Défis ne pourra être tenue responsable de dommages matériels liés à l’utilisation du site. De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour.</li>
</ul>
</div>
<div class="col sml-hide med-1 med-show lrg-1 lrg-show"></div>
</div>