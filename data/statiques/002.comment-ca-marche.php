<div class="grid colorwhite">

<!--Affichage grand écran-->
<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>
<div class="col sml-11 sml-show med-6 med-show lrg-5 lrg-show">
<div class="flex230">
<div class="encartyellow">
<h1>Comment s'y prendre ?</h1>

<p>Pour mener à bien la mise en place de votre site internet, il est indispensable de respecter certaines étapes.</p>
<p>Un projet bien réfléchi est un projet optimisé !</p>

</div>
</div>
</div>

<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

<div class="col sml-hide med-hide lrg-5 lrg-show"><img alt="" class="img400" src="data/medias/ccm_400-230-1.svg" role="img" /></div>
<div class="col sml-hide med-6 med-show lrg-hide"><img alt="" class="img250" src="data/medias/ccm_400-230-1.svg" role="img" /></div>

</div>

<!-- Deuxième contenu -->

<div class="grid colorgrey">

<!--Affichage grand écran-->
<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

<div class="col sml-hide med-hide lrg-5 lrg-show"><img alt="" class="img400" src="data/medias/ccm_400-230-1.svg" role="img" /></div>
<div class="col sml-hide med-6 med-show lrg-hide"><img alt="" class="img250" src="data/medias/ccm_400-230-1.svg" role="img" /></div>

<div class="col sml-hide med-6 med-show lrg-5 lrg-show">
<div class="flex230">
<div class="encart">
<h1>1- Remplir un cahier des charges</h1>

<p>Un cahier des charges permet de cibler l'ensemble de ses besoins, de ses attentes... un grand pas en avant dans la formalisation de son projet.</p>
<p>Merci de bien vouloir remplir le formulaire ci dessous. Nous vous recontacterons très rapidement.</p>
<a class="button download"  href="https://framaforms.org/contenu-de-votre-site-vitrine-mini-site-1593603857">Formulaire en ligne</a>

</div>
</div>
</div>

<!--Affichage smartphone-->
<div class="col sml-11 sml-show med-hide lrg-hide">
<div class="flex450">
<div class="encart">
<h1>1- Remplir un cahier des charges</h1>

<p>Un cahier des charges permet de cibler l'ensemble de ses besoins, de ses attentes... un grand pas en avant dans la formalisation de son projet.</p>
<p>Merci de bien vouloir remplir le formulaire ci dessous. Nous vous recontacterons très rapidement.</p>
<a class="button download"  href="https://framaforms.org/contenu-de-votre-site-vitrine-mini-site-1593603857">Formulaire en ligne</a>

</div>
</div>
</div>

<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

</div>
