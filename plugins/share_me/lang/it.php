<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Abilita i cookie",
	'L_CC_DISMISS'					=> "Accettare",
	'L_CC_LINK'						=> "Per saperne di più",
	'L_CC_MESSAGE'					=> "Questo sito Web utilizza i cookie per assicurarti di ottenere la migliore esperienza sul nostro sito web",
	'L_CHAPO'						=> "Filtra gli articoli con l'intestazione",
	'L_CHAPO_HINT'					=> "Per le pagine della homepage, delle categorie e dei tag, non visualizzare i pulsanti dei social network se l'articolo ha un'intestazione.",
	'L_COOKIE'						=> "Richiedi i cookie",
	'L_COOKIE_POLICY'				=> "Pagina statica per la politica dei cookie",
	'L_COOKIE_POLICY_NO'			=> "No",
	'L_DRAG_AND_DROP'				=> "Ordina le reti spostando le icone sopra",
	'L_IMAGE_INFO'					=> "Circa la dimensione delle immagini",
	'L_MEDIA'						=> "Immagine predefinita",
	'L_MEDIA_TITLE'					=> "Sfoglia la cartella dei media",
	'L_OGP_DEBUGGER'				=> "Debugger Open Graph di Facebook",
	'L_OPENGRAPH'					=> "Ulteriori informazioni sul protocollo Open Graph",
	'L_SAVE'						=> "Salvare",
	'L_TAGS'						=> "Condividi i tag"
);
?>
