<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Schakel de cookies in",
	'L_CC_DISMISS'					=> "Aanvaarden",
	'L_CC_LINK'						=> "Kom meer te weten",
	'L_CC_MESSAGE'					=> "Deze website maakt gebruik van cookies om ervoor te zorgen dat u de beste ervaring op onze website krijgt",
	'L_CHAPO'						=> "Filter de artikelen met kop",
	'L_CHAPO_HINT'					=> "Voor homepagina's, categorieën en tagspagina's, geeft u sociale-netwerkenknoppen niet weer als het artikel een koptekst heeft.",
	'L_COOKIE'						=> "Vraag om cookies",
	'L_COOKIE_POLICY'				=> "Statische pagina voor het cookiebeleid",
	'L_COOKIE_POLICY_NO'			=> "Nee",
	'L_DRAG_AND_DROP'				=> "Sorteer de netwerken door de bovenstaande pictogrammen te verplaatsen",
	'L_IMAGE_INFO'					=> "Over de grootte van de afbeeldingen",
	'L_MEDIA'						=> "Standaard afbeelding",
	'L_MEDIA_TITLE'					=> "Blader door de map Medias",
	'L_OGP_DEBUGGER'				=> "Open Graph debugger door Facebook",
	'L_OPENGRAPH'					=> "Meer informatie over het Open Graph-protocol",
	'L_SAVE'						=> "Opslaan",
	'L_TAGS'						=> "Deel de tags"
);
?>
