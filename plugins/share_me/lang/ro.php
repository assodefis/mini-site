<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Activați cookie-urile",
	'L_CC_DISMISS'					=> "Accept",
	'L_CC_LINK'						=> "Aflați mai multe",
	'L_CC_MESSAGE'					=> "Acest site folosește cookie-uri pentru a vă asigura că beneficiați de cea mai bună experiență pe site-ul nostru",
	'L_CHAPO'						=> "Filtrați articolele cu antet",
	'L_CHAPO_HINT'					=> "Pentru paginile de pornire, categorii și etichete, nu afișați butoanele rețelelor sociale dacă articolul are un antet.",
	'L_COOKIE'						=> "Cereți cookie-uri",
	'L_COOKIE_POLICY'				=> "Pagina statică pentru politica de cookie-uri",
	'L_COOKIE_POLICY_NO'			=> "Nu",
	'L_DRAG_AND_DROP'				=> "Sortați rețelele mutând pictogramele de mai sus",
	'L_IMAGE_INFO'					=> "Despre dimensiunea imaginilor",
	'L_MEDIA'						=> "Imaginea prestabilită",
	'L_MEDIA_TITLE'					=> "Răsfoiți folderul medias",
	'L_OGP_DEBUGGER'				=> "Open debugger Graph de Facebook",
	'L_OPENGRAPH'					=> "Aflați mai multe despre protocolul Open Graph",
	'L_SAVE'						=> "Salvați",
	'L_TAGS'						=> "Distribuiți etichetele"
);
?>
