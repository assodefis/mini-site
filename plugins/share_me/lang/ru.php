<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Включить куки",
	'L_CC_DISMISS'					=> "принимать",
	'L_CC_LINK'						=> "Учить больше",
	'L_CC_MESSAGE'					=> "Этот веб-сайт использует куки-файлы, чтобы обеспечить вам лучший опыт на нашем веб-сайте.",
	'L_CHAPO'						=> "Фильтровать статьи с заголовком",
	'L_CHAPO_HINT'					=> "Для домашней страницы, категорий и тегов страниц не показывайте кнопки социальных сетей, если статья имеет заголовок.",
	'L_COOKIE'						=> "Попросить печенье",
	'L_COOKIE_POLICY'				=> "Статическая страница для политики куки",
	'L_COOKIE_POLICY_NO'			=> "нет",
	'L_DRAG_AND_DROP'				=> "Сортируйте сети, перемещая значки выше",
	'L_IMAGE_INFO'					=> "О размере изображений",
	'L_MEDIA'						=> "Изображение по умолчанию",
	'L_MEDIA_TITLE'					=> "Просмотрите папку с носителями",
	'L_OGP_DEBUGGER'				=> "Отладчик Open Graph от Facebook",
	'L_OPENGRAPH'					=> "Узнайте больше о протоколе Open Graph",
	'L_SAVE'						=> "Сохранить",
	'L_TAGS'						=> "Поделитесь тегами"
);
?>
