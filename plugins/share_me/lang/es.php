<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Habilitar las cookies",
	'L_CC_DISMISS'					=> "Aceptar",
	'L_CC_LINK'						=> "Aprende más",
	'L_CC_MESSAGE'					=> "Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web.",
	'L_CHAPO'						=> "Filtrar los artículos con encabezado.",
	'L_CHAPO_HINT'					=> "Para la página de inicio, categorías y etiquetas, no muestre los botones de redes sociales si el artículo tiene un encabezado.",
	'L_COOKIE'						=> "Pedir galletas",
	'L_COOKIE_POLICY'				=> "Página estática para la política de cookies.",
	'L_COOKIE_POLICY_NO'			=> "No",
	'L_DRAG_AND_DROP'				=> "Ordena las redes moviendo los iconos de arriba",
	'L_IMAGE_INFO'					=> "Sobre el tamaño de las imágenes.",
	'L_MEDIA'						=> "Imagen por defecto",
	'L_MEDIA_TITLE'					=> "Navegar por la carpeta de medios",
	'L_OGP_DEBUGGER'				=> "Open Graph debugger de Facebook",
	'L_OPENGRAPH'					=> "Aprenda más sobre el protocolo Open Graph",
	'L_SAVE'						=> "Salvar",
	'L_TAGS'						=> "Compartir las etiquetas"
);
?>
