<?php
if (! defined('PLX_ROOT')) exit;
?>
<div id="help_share_me">
<p>
	Ce plugin vous permet de rajouter une série de liens pour partager un article ou une page statique via les réseaux sociaux.
</p>
<p>
	Il n'utilise aucun script javascript proposé par les réseaux sociaux. En effet, il est fréquent que ceux-ci rajoutent un cookie à vos pages pour suivre le parcours de vos visiteurs.
</p><p>
	Toutes les informations utiles aux réseaux sociaux sont rajoutées dans l'entête de vos pages avec les balises <strong>meta</strong> définies par le protocole <strong><a href="http://opengraphprotocol.org/" target="_blank"> Opengraph</a></strong>. Ces balises sont utilisées par les réseaux sociaux pour compléter l'information donnée par l'URL utilisée pour le partage sur sur les réseaux sociaux. S'il existe un lien vers une image dans le contenu de votre page, celui sera proposé en partage.
</p>
<p>
	Pour utiliser ce plugin, il suffit d'ajouter un appel pour le hook "share_me" sur les modèles de page article ou static de votre thème.
</p>
<pre><code>// par exemple pour article.php
&lt;h1>&lt;?php $plxShow->artTitle(); ?>&lt;/h1>
&lt;?php eval($plxShow->callHook('share_me')); ?></code></pre>
<pre><code>// par exemple pour static.php:
&lt;h1>&lt;?php &dollar;plxShow->staticTitle(); ?> ?>&lt;/h1>
&lt;?php eval(&dollar;plxShow->callHook('share_me')); ?></code></pre>
<p>
	Il est également possible de passer en paramètre un lien vers un média en adresse relative à l'adresse du site.
</p>
<pre><code>&lt;?php eval($plxShow->callHook('share_me', 'data/medias/moi.jpg')); ?></code></pre>
<p>
	Depuis sa version 5.5, Pluxml propose d'associer une image d'accroche à chaque article. Si elle existe, elle sera partagée sur les réseaux sociaux.
	Dans le cas contrainte, le plugin recherchera une image dans le contenu de l'article.
</p>
<p>
	Si l'article a un chapô, celui-ci sera partagé sur les réseaux sociaux. Dans le cas contraire, le plugin essaiera de le remplacer par le contenu de la balise meta-description.
</p>
<p>
	Il en va de même pour les pages statiques, hormis qu'elles n'ont pas d'image d'accroche.
</p>
<p>
	Il est proposé un partage vers les réseaux sociaux suivants :
</p>
	<ul>
<?php
		$networks = array(
			'twitter'	=>'http://twitter.com/',
			'facebook'	=>'https://www.facebook.com/',
			'googleplus'=>'https://plus.google.com/',
			'linkedin'	=>'https://fr.linkedin.com/',
			'pinterest'	=>'https://fr.pinterest.com/',
			'diaspora'	=>'https://diasporafoundation.org/'
		);
		$root = PLX_PLUGINS.$page.'/icons/';
		foreach ($networks as $key=>$ref) {
			$title = str_replace('plus', '+', ucfirst($key));
			echo <<< NETWORK
			<li><a href="$ref" title="$title"><img src="$root$key.svg" alt="$title" /></a></li>

NETWORK;
}
?>
	</ul>
	<p>
	Pour Twitter, on peut préciser le compte qui diffuse le tweet (via).
	</p>
	<p>
	On peut également partager par courriel.
	</p>
	<p>
		On peut voir comment l'article ou la page statique seront partagés sur les réseaux avec le debugger suivant:<br>
		<a href="https://www.facebook.com/login.php?next=https%3A%2F%2Fdevelopers.facebook.com%2Ftools%2Fdebug%2F" referrer="noreferrer" target="_blank">https://www.facebook.com/login.php?next=https://developers.facebook.com/tools/debug/</a>
	</p>
</p>
</div>