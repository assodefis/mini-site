<?php
$LANG = array(
	'L_CC_ALLOW'					=> "Włącz pliki cookie",
	'L_CC_DISMISS'					=> "Zaakceptować",
	'L_CC_LINK'						=> "Ucz się więcej",
	'L_CC_MESSAGE'					=> "Ta strona korzysta z plików cookie, aby zapewnić Ci najlepszą jakość na naszej stronie",
	'L_CHAPO'						=> "Filtruj artykuły z nagłówkiem",
	'L_CHAPO_HINT'					=> "W przypadku stron na stronie głównej, kategorii i tagów nie wyświetlaj przycisków sieci społecznościowych, jeśli artykuł ma nagłówek.",
	'L_COOKIE'						=> "Zapytaj o ciasteczka",
	'L_COOKIE_POLICY'				=> "Strona statyczna dla zasad dotyczących plików cookie",
	'L_COOKIE_POLICY_NO'			=> "Nie",
	'L_DRAG_AND_DROP'				=> "Posortuj sieci, przesuwając powyższe ikony",
	'L_IMAGE_INFO'					=> "O wielkości zdjęć",
	'L_MEDIA'						=> "Domyślny obraz",
	'L_MEDIA_TITLE'					=> "Przeglądaj folder mediów",
	'L_OGP_DEBUGGER'				=> "Debugger Open Graph firmy Facebook",
	'L_OPENGRAPH'					=> "Dowiedz się więcej o protokole Open Graph",
	'L_SAVE'						=> "Zapisać",
	'L_TAGS'						=> "Udostępnij tagi"
);
?>
