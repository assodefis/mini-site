(function(theClassName) {

  'use strict';

  // building some useful constants
  const ACCOUNTS = JSON.parse('{'+document.getElementById("ACCOUNTS").value+'}');
  const NETWORKS = JSON.parse('{'+document.getElementById("NETWORKS").value+'}');

  // callback function in response on click event
  function popup(event) {
    if(event.target.tagName == 'IMG') {
      var network = event.target.getAttribute('data');
      if(network !== null) {
        if(network in NETWORKS) {
          event.preventDefault();
          var datas = this.dataset;
          var nw = NETWORKS[network];
          var href = nw.url;
          var matches = href.match(/#\w+#/g);
          if(matches !== null) {
            matches.forEach(function(tag) {
              if(tag == '#account#') {
                // for some network, it's better to give an account, e.g.: Twitter, Facebook
                if(network in ACCOUNTS) {
                  href = href.replace(tag, ACCOUNTS[network], href);
                }
              } else {
                var key = tag.substring(1, tag.length-1);
                var newValue = (key in datas) ? datas[key] : '';
                href = href.replace(tag, newValue);
              }
            });
            href = encodeURI(href);
            var top = (screen.height - nw.h) / 2, left = (screen.width - nw.w) / 2;
            var options = 'menubar=no, toolbar=no, resizable=yes, scrollbars=no, width='+nw.w+', height='+nw.h+', top='+top+', left='+left;
            window.open(href, '', options);
          }
        } else {
          console.log('Unknown social network: ' + network);
        }
      }
    }
  };

  // Add eventListeners at every element with theClassName class
  const toolbars = document.getElementsByClassName(theClassName);
  if(toolbars.length > 0) {
    for(var i=0, iMax=toolbars.length; i<iMax; i++) {
      toolbars.item(i).addEventListener('click', popup);
    }
  }
})('social-buttons');

document.addEventListener('DOMContentLoaded', function () {
  document.getElementById('PRINT')
          .addEventListener('click', function PRINT() { window.print(); return false; });
});
