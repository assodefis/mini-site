var data = {
  "name": "Share to Diaspora*",
  "description": "Diaspora* is a social network where you keep control.",
  "author": "kemenaran",
  "homepageURL": "https://sharetodiaspora.github.io/about/",
  "iconURL":   "https://sharetodiaspora.github.io/assets/diaspora-icon-16.png",
  "icon32URL": "https://sharetodiaspora.github.io/assets/diaspora-icon-32.png",
  "icon64URL": "https://sharetodiaspora.github.io/assets/diaspora-icon-64.png",
  "shareURL": "https://sharetodiaspora.github.io/?url=%{url}&title=%{title}&notes=%{text}",
  "version": "1.0.0"
};
function activateSocialFeature(node) {
  var event = new CustomEvent("ActivateSocialFeature");
  node.setAttribute("data-service", JSON.stringify(data));
  node.dispatchEvent(event);
}
