window.addEventListener('load', function(){
  window.cookieconsent.initialise({
    palette: {
      popup:	{ background: '#209967' },
      button:	{ background: '#0f3a69' }
    },
    position: 'bottom-right',
    theme: 'classic',
    content: {
      message: document.getElementById("l_cc_message").value,
      dismiss: document.getElementById("l_cc_dismiss").value,
      'link': document.getElementById("l_cc_link").value,
      href: document.getElementById("l_cc_href").value
    }
  })
});
