<?php
if (! defined('PLX_ROOT')) exit;

# Control du token du formulaire
plxToken::validateFormToken($_POST);

/* Save the parameters of the plugin after posting the form. */
if (!empty($_POST)) {
	// Saving account of every network
	foreach (array_keys(share_me::NETWORKS) as $id) {
		$accountName = 'account_' . $id;
		$value = filter_input(INPUT_POST, $accountName, FILTER_SANITIZE_STRING);
		if(!empty($value)) {
			$plxPlugin->setParam($accountName, trim($value), 'cdata');
		} else {
			$plxPlugin->delParam($accountName);
		}
	}

	/* Save every set of checkboxes for networks and extras */
	$extras = explode(' ', share_me::EXTRAS);
	$callbacks = array(
		'networks'	=> function($item) use($plxPlugin) {
					return array_key_exists($item, share_me::NETWORKS);
		},
		'extras'	=> function($item) use($extras) {
					return in_array($item, $extras);
		}
	);
	foreach(array_keys($callbacks) as $dataset) {
		$datas = (!empty($_POST[$dataset])) ? array_filter(array_keys($_POST[$dataset]), $callbacks[$dataset]) : false;
		if(!empty($datas)) {
			$plxPlugin->setParam($dataset, implode('|', $datas), 'string');
		} else {
			$plxPlugin->delParam($dataset);
		}
	}

	/* Some texts typing by the user */
	foreach(explode(' ', 'media cookie_policy') as $field) {
		$value = trim(filter_input(INPUT_POST, $field, FILTER_SANITIZE_STRING));
		if(!empty($value))
			$plxPlugin->setParam($field, $value, 'cdata'); // may be an email address
		else
			$plxPlugin->delParam($field);
	}

	$plxPlugin->saveParams();
	header('Location: parametres_plugin.php?p=' . $plugin);
	exit;
}

/** Print a select of active static-pages ordered by groups */
function printSelectStaticPages($name) {
	global $plxPlugin, $plxAdmin;
?>
				<select name="<?= $name; ?>">
					<option value=""><?php $plxPlugin->lang('L_COOKIE_POLICY_NO'); ?></option>
<?php
	$buf = array_filter(
		$plxAdmin->aStats,
		function($item) { return !empty($item['active']); }
	);
	uasort($buf, function($a, $b) {
		$aG = trim($a['group']);
		$bG = trim($b['group']);
		if((empty($aG) and empty($bG)) or ($aG == $bG))
			return strcmp(trim($a['name']), trim($b['name']));
		else
			return strcmp($aG, $bG);
	});
	$group = '';
	$current = $plxPlugin->getParam('cookie_policy');
	foreach($buf as $id=>$infos) {
		$caption = $infos['name'];
		if($group != $infos['group']) {
			$separation = (!empty($group)) ? '</optgroup>' : '';
			$separation .= (!empty($infos['group'])) ? '<optgroup label="'.$infos['group'].'">' : '';
				echo <<< EOT
					$separation

EOT;
		$group = $infos['group'];
		}
		$value = intval($id).'/'.$infos['url']; // see plxShow::staticList()
		$selected = ($value == $current) ? ' selected' : '';
		echo <<< EOT
					<option value="$value"$selected>$caption</option>

EOT;
	}
	if(!empty($group)) {

?>
					</optgroup>
<?php
	}
?>
				</select>

<?php
}
/* ----------- the form  is starting here ------------ */
?>

	<form id="form_<?= $plugin; ?>" method="post">
		<p class="in-action-bar">
			<?= plxToken::getTokenPostMethod() ?>
			<input type="submit" value="<?php $plxPlugin->lang('L_SAVE') ?>"/>
		</p>
		<p class="center">
			<a href="http://ogp.me" referrer="noreferrer" target="_blank"><?php $plxPlugin->lang('L_OPENGRAPH'); ?></a>
			<img id="logo-opengraph" src="<?= PLX_PLUGINS . $plugin; ?>/icon.png" alt="logo Opengraph">
		</p>
		<p class="center"><em><?php $plxPlugin->lang('L_DRAG_AND_DROP'); ?></em></p>
		<div class="networks">
<?php /* networks */
/** ordered list of networks selected by the user */
$networksStr = trim($plxPlugin->getParam('networks'));
if(!empty($networksStr)) {
	$networks = explode('|', $networksStr);
	$orderedNetworks = $networks;
	foreach(array_keys(share_me::NETWORKS) as $network) {
		if(!in_array($network, $orderedNetworks)) {
			$orderedNetworks[] = $network;
		}
	}
} else {
	$orderedNetworks = array_keys(share_me::NETWORKS);
}

foreach ($orderedNetworks as $id) {
	// checks if $id is a known network
	if(!in_array($id, array_keys(share_me::NETWORKS))) {
		echo "<!-- passed: $id -->\n";
		continue;
	}
	$checked = (!empty($networks) and in_array($id, $networks)) ? ' checked' : '';
?>
			<p>
				<label for="id_<?= $id; ?>"><?= ($id != 'google_p') ? ucfirst($id): 'Google+'; ?></label>
				<input id="id_<?= $id; ?>" name="networks[<?= $id; ?>]" type="checkbox" value="1"<?= $checked; ?> />
				<?= $plxPlugin->iconNetwork($id); ?>
<?php
	if (in_array($id, share_me::ACCOUNT_NETWORKS)) {
		$value = $plxPlugin->getParam("account_$id");
?>
				<input id="id_account_<?= $id ?>" name="account_<?= $id ?>" value="<?= $value ?>" maxlength="30" type="text">
<?php
	}
?>
			</p>
<?php
}
?>
		</div><div class="extras">
<?php	/** List of checkboxes for extras choices */
$params = explode('|', $plxPlugin->getParam('extras'));
foreach(explode(' ', share_me::EXTRAS) as $field) {
	$checked = (in_array($field, $params)) ? ' checked' : '';
?>
			<p>
				<label for="id_<?= $field; ?>"><?= $plxPlugin->lang('L_'.strtoupper($field)); ?>
<?php		if($field == 'chapo') {
				$caption = $plxPlugin->getLang('L_'.strtoupper($field).'_HINT');
				echo <<< HINT
				<a class="hint"><span>$caption</span></a>\n
HINT;
			}
?>
				</label>
				<input id="id_<?= $field; ?>" name="extras[<?= $field; ?>]" type="checkbox" value="1"<?= $checked; ?> />
			</p>
<?php
}
?>
		</div>
		<div>
			<p class="center">
				<label><?php $plxPlugin->lang('L_COOKIE_POLICY'); ?></label>
<?php printSelectStaticPages('cookie_policy'); ?>
			</p>
		</div>
		<p class="expand">
			<label for="id_media"><?php $plxPlugin->LANG('L_MEDIA'); ?></label>
			<?php plxUtils::printInput('media', $plxPlugin->getParam('media'), 'text'); ?>
			<input id="media_selection" type="button" value="&#x1f5c1;" title="<?= $plxPlugin->lang('L_MEDIA_TITLE'); ?>"/>
		</p>
		<p id="id_media_img">
<?php
$img = $plxPlugin->getParam('media');
if(!empty($img)) {
?>
			<img src="<?= PLX_ROOT.$img; ?>">
<?php
}
?>
		</p>
		<p class="center">
<?php
/** Some links @Facebook for debugging the generated HTML pages */
$facecookDevs = array(
	'L_IMAGE_INFO'		=> 'docs/sharing/best-practices/#images',
	'L_OGP_DEBUGGER'	=> 'tools/debug/'
);
foreach($facecookDevs as $caption=>$url) {
?>
			<a href="https://developers.facebook.com/<?= $url; ?>" referrer="noreferrer" target="_blank"><?php $plxPlugin->lang($caption); ?></a>
<?php
}
?>
		</p>
	</form>
	<script type="text/javascript">
		(function(){
			var selector = document.getElementById('media_selection');
			if(selector != null) {
				selector.addEventListener('click', function(event) {
					mediasManager.openPopup('id_media', true);
				});
			}
<?php
if(version_compare('5.6', PLX_VERSION) > 0) {
?>
			var inputs = document.querySelectorAll('input[type="text"][size]');
			if(inputs != null) {
				inputs.forEach(function(item) { item.removeAttribute('size'); });
			}
<?php
}
?>
		})();
	</script>
	<script type="text/javascript" src="<?= PLX_PLUGINS . $plugin; ?>/drag-and-drop.js"></script>
