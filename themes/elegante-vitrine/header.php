<?php if (!defined('PLX_ROOT')) exit; ?>
<!DOCTYPE html>
<html lang="<?php $plxShow->defaultLang() ?>">
<head>
	<meta charset="<?php $plxShow->charset('min'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
	<title><?php $plxShow->pageTitle(); ?></title>
	<?php $plxShow->meta('description') ?>
	<?php $plxShow->meta('keywords') ?>
	<?php $plxShow->meta('author') ?>
	<link rel="icon" href="<?php $plxShow->template(); ?>/img/favicon.png" />
	<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/css/plucss.min.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/css/theme.min.css" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/css/print.min.css" media="print"/>

	<?php $plxShow->templateCss('css/') ?>
	<?php $plxShow->pluginsCss()
?>
<?php $plxShow->artNavigation("\t" . '<link rel="#dir" href="#url" />'); ?>
<?php $plxShow->canonical(); ?>
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />
</head>

<body id="top" class="page mode-<?php $plxShow->mode(true) ?>">

	<header class="header sticky">

		<div class="container">

			<div class="grid">

				<div class="logo col sml-hide med-hide lrg-4 lrg-show">
					<a href="<?php $plxShow->racine(); ?>"><img src="data/medias/logo.svg" alt="Logo"></a>
					<h1><?php $plxShow->mainTitle('link'); ?></h1>
				</div>

				<div class="logo col sml-4 sml-show med-2 med-show lrg-hide">
					<a href="<?php $plxShow->racine(); ?>"><img src="data/medias/logo.svg" alt="Logo"></a>
				</div>

				<div class="col sml-8 sml-show med-10 med-show lrg-8 lrg-show">

					<nav class="nav">

						<div class="responsive-menu">
							<label for="menu"></label>
							<input type="checkbox" id="menu" aria-label="menu">
							<ul class="menu">
								<?php $plxShow->staticList($plxShow->getLang('HOME'),'<li class="#static_class #static_status" id="#static_id"><a href="#static_url" title="#static_name">#static_name</a></li>'); ?>
								<!--<?php $plxShow->pageBlog('<li class="#page_class #page_status" id="#page_id"><a href="#page_url" title="#page_name">#page_name</a></li>'); ?>-->
								<?php $plxShow->catList('','<li class="static menu #cat_status" id="#cat_id"><a href="#cat_url" title="#cat_name">#cat_name</a></li>'); ?>
								<?php $classcontact="noactive";
								if ($plxMotor->path_url == "index.php?contact") {
									$classcontact="active";
								} ?>
								<li class="static menu <?php echo $classcontact ?>"><a href="index.php?contact">Contact</a></li>
							</ul>
						</div>

					</nav>

				</div>

			</div>

		</div>

	</header>
