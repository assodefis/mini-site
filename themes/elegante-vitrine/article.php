<?php if ($plxShow->catId() == '001') { ?>
<?php include(dirname(__FILE__) . '/header.php'); ?>

	<main class="main">

		<div class="container">

			<div class="grid">

				<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

				<div class="content col sml-12 med-12 lrg-10">


					<article class="article" id="post-<?php echo $plxShow->artId(); ?>">

						<header>
							<span class="art-date">
								<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
									<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
								</time>
							</span>
							<h2>
								<?php $plxShow->artTitle(); ?>
							</h2>
							<div>
								<small>
									<span class="written-by">
										<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
									</span>
									<span class="art-nb-com">
										<a href="<?php $plxShow->artUrl(); ?>#comments" title="<?php $plxShow->artNbCom(); ?>"><?php $plxShow->artNbCom(); ?></a>
									</span>
								</small>
							</div>
							<!--<div>
								<small>
									<span class="classified-in">
										<?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat() ?>
									</span>
									<span class="tags">
										<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
									</span>
								</small>
							</div>-->
						</header>

						<?php $plxShow->artThumbnail(); ?>
						<?php $plxShow->artContent(); ?>

					</article>

					<div class="text-center">
						<?php eval($plxShow->callHook('share_me')); ?>
						<?php $plxShow->artAuthorInfos('<div class="author-infos">#art_authorinfos</div>'); ?>
						<?php include(dirname(__FILE__).'/commentaires.php'); ?>
					</div>

				</div>
				<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

			</div>

		</div>

	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
<?php } ?>

<?php if ($plxShow->catId() == '002') { ?>
	<?php if (!defined('PLX_ROOT')) exit; ?>
	<!DOCTYPE html>
	<html lang="<?php $plxShow->defaultLang() ?>">
	<head>
		<meta charset="<?php $plxShow->charset('min'); ?>">
		<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
		<title><?php $plxShow->pageTitle(); ?></title>
		<?php $plxShow->meta('description') ?>
		<?php $plxShow->meta('keywords') ?>
		<?php $plxShow->meta('author') ?>
		<link rel="icon" href="<?php $plxShow->template(); ?>/img/favicon.png" />
		<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/css/plucss.min.css" media="screen"/>
		<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/css/theme.min.css" media="screen"/>
		<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/css/print.min.css" media="print"/>

		<?php $plxShow->templateCss('css/') ?>
		<?php $plxShow->pluginsCss()
?>
<?php $plxShow->artNavigation("\t" . '<link rel="#dir" href="#url" />'); ?>
<?php $plxShow->canonical(); ?>
		<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
		<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />
	</head>

	<body id="top" class="cat-gallery page mode-<?php $plxShow->mode(true) ?>">

		<div class="container">
			<div class="grid">

					<article class="article" id="post-<?php echo $plxShow->artId(); ?>">

						<div class="col sml-hide med-2 med-show lrg-2 lrg-show">
							<h2>
								<?php $plxShow->artTitle('link'); ?>
							</h2>
							<small>
							<span class="art-date">
								<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
									<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
								</time>
							</span></br>
								<span class="written-by">
									<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
								</span></br>
								<span class="tags">
									<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
								</span></br>
								<span class="art-nb-com">
									<a href="<?php $plxShow->artUrl(); ?>#comments" title="<?php $plxShow->artNbCom(); ?>"><?php $plxShow->artNbCom(); ?></a>
								</span>
							</small>
							<?php eval($plxShow->callHook('share_me')); ?>
						</div>

						<div class="image col sml-12 med-8 lrg-8">
							<div class="art-navigation-overlay">
								<ul class="art-navigation">
									<?php
											$plxShow->artNavigation('<li><a href="#url" rel="#dir" title="#title">#icon</a></li>', 'prev');
									?>
								</ul>
							</div>
							<img src="<?php echo $plxMotor->urlRewrite(trim($plxMotor->plxRecord_arts->f('thumbnail'))); ?>" alt="<?php echo $plxMotor->plxRecord_arts->f('thumbnail_alt'); ?>">
							<div class="art-navigation-overlay">
								<ul class="art-navigation close">
								<?php
										$plxShow->artNavigation('<li><a href="#url" rel="#dir" title="#title">#icon</a></li>', 'up next up');
								?>
								</ul>
							</div>
						</div>

						<div class="col sml-12 sml-show med-hide lrg-hide">
							<h2>
								<?php $plxShow->artTitle('link'); ?>
							</h2></br>
							<small>
							<span class="art-date">
								<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
									<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
								</time>
							</span></br>
								<span class="written-by">
									<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
								</span></br>
								<span class="tags">
									<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
								</span></br>
								<span class="art-nb-com">
									<a href="<?php $plxShow->artUrl(); ?>#comments" title="<?php $plxShow->artNbCom(); ?>"><?php $plxShow->artNbCom(); ?></a>
								</span>
							</small>
							<?php eval($plxShow->callHook('share_me')); ?>
						</div>

						<div class="col sml-hide med-2 med-show lrg-m2 lrg-show">
							<?php $plxShow->artContent(); ?>
						</div>

						<div class="col sml-12 sml-show med-hide lrg-hide">
							<?php $plxShow->artContent(); ?>
						</div>

					</article>
				</div>

				<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>
				<div class="content col sml-12 med-12 lrg-10">
					<div class="grid">
						<?php $plxShow->artAuthorInfos('<div class="author-infos">#art_authorinfos</div>'); ?>

			<div class="art-navigation-overlay range">
				<?php $plxShow->artNavigationRange(); ?>
			</div>

						<?php include(dirname(__FILE__).'/commentaires.php'); ?>
					</div>
				</div>
				<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>
	</main>
</body>
</html>
<?php } ?>
