<?php include(dirname(__FILE__) . '/header.php'); ?>

	<main class="main">

		<div class="container">

					<article class="article static" id="static-page-<?php echo $plxShow->staticId(); ?>">

						<?php $plxShow->staticContent(); ?>

					</article>

		</div>

	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
