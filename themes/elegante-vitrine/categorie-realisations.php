<?php include(dirname(__FILE__).'/header.php'); ?>

	<main class="main" role="main">

		<div class="container cat-gallery">

					<div class="grid">

							<?php while($plxShow->plxMotor->plxRecord_arts->loop()): ?>

							<div class="col sml-11 med-4 lrg-4">

								<article class="article" id="post-<?php echo $plxShow->artId(); ?>">


										<div class="gal-thumb">
											<?php $plxShow->artThumbnail('<a href="#img_url"><img class="art_thumbnail" src="#img_thumb_url" alt="#img_alt" title="#img_title" /></a>', true, true); ?>
										</div>
										<h2>
											<?php $plxShow->artTitle('link'); ?>
										</h2>
										<span class="gal-chapo">
											<?php $plxShow->artChapo(''); ?>
										</span>

								</article>

							</div>
							<?php endwhile; ?>

					</div>

			<nav class="pagination text-center">
				<?php $plxShow->pagination(); ?>
			</nav>

			<span>
				<?php $plxShow->artFeed('rss',$plxShow->catId()); ?>
			</span>

		</div>

	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
