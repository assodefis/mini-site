<?php include(dirname(__FILE__).'/header.php'); ?>

	<main class="main" role="main">

		<div class="container">
			<!-- Récupération URL de la page, ID de la catégorie et numéro de dernière pqge-->
			<?php $urlPage = $plxMotor->racine.$plxMotor->path_url;
			$cat = ($plxShow->plxMotor->mode == 'categorie') ? $plxShow->plxMotor->cible : '\d{3}';
			$last_page = ceil($plxMotor->nbArticles('(?:home,|\d{3},)*' . $cat . '(?:,\d{3}|,home)*', '\d{3}', '', 'before') / $plxMotor->bypage);
			?>

			<div class="grid">

				<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

				<div class="col sml-12 med-12 lrg-11">

					<ul class="repertory menu breadcrumb">
						<li><a href="<?php $plxShow->racine() ?>"><?php $plxShow->lang('HOME'); ?></a></li>
						<li><?php $plxShow->catName(); ?></li>
					</ul>

					<p><?php $plxShow->catDescription('#cat_description'); ?></p>
				</div>
			</div>

			<?php if ($urlPage == $plxShow->catUrl($plxShow->catId()) OR $urlPage == $plxShow->catUrl($plxShow->catId()).'/page1') {
				$i=0; while($i<2 AND $plxShow->plxMotor->plxRecord_arts->loop()): ?>
					<div class="grid">
						<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>
						<div class="col sml-12 med-12 lrg-10">

							<article class="article" id="post-<?php echo $plxShow->artId(); ?>">

								<header>
									<span class="art-date">
										<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
											<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
										</time>
									</span>
									<h2>
										<?php $plxShow->artTitle('link'); ?>
									</h2>
									<div>
										<small>
											<span class="written-by">
												<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
											</span>
											<span class="art-nb-com">
												<?php $plxShow->artNbCom(); ?>
											</span>
										</small>
									</div>
									<div>
										<small>
											<!--<span class="classified-in">
												<?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat() ?>
											</span>
											<span class="tags">-->
												<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
											</span>
										</small>
									</div>
								</header>

								<?php $plxShow->artThumbnail('<a href="#img_url"><img class="art_thumbnail" src="#img_thumb_url" alt="#img_alt" title="#img_title" /></a>', true, true); ?>
								<?php $plxShow->artChapo(); ?>

							</article>
						</div>
						<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

					</div>
					<?php $i++; ?>
				<?php endwhile; } ?>

		</div>

		<div class="container">
			<?php	if (!($urlPage == $plxShow->catUrl($plxShow->catId()).'/page'.$last_page)) { ?>

			<?php for ($k=0; $k <= 2; $k++) { ?>
				<div class="grid">
					<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

						<?php $l=0; while($l<2 AND $plxShow->plxMotor->plxRecord_arts->loop()): ?>

							<div class="col sml-12 med-6 lrg-5">

								<article class="article" id="post-<?php echo $plxShow->artId(); ?>">

									<header>
										<span class="art-date">
											<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
												<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
											</time>
										</span>
										<h2>
											<?php $plxShow->artTitle('link'); ?>
										</h2>
										<div>
											<small>
												<span class="written-by">
													<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
												</span>
												<span class="art-nb-com">
													<?php $plxShow->artNbCom(); ?>
												</span>
											</small>
										</div>
										<div>
											<small>
												<!--<span class="classified-in">
													<?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat() ?>
												</span>-->
												<span class="tags">
													<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
												</span>
											</small>
										</div>
									</header>

									<div class="thumb-cat">
										<?php $plxShow->artThumbnail('<a href="#img_url"><img class="art_thumbnail" src="#img_thumb_url" alt="#img_alt" title="#img_title" /></a>', true, true); ?>
									</div>

									<?php $plxShow->artChapo(); ?>

								</article>

							</div>
						<?php $l++; ?>
					<?php endwhile; ?>
					<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

				</div>
			<?php } ?>
		<?php } else {
			while($plxShow->plxMotor->plxRecord_arts->loop()): ?>
				<div class="grid">
					<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>
					<div class="col sml-12 med-12 lrg-10">

						<article class="article" id="post-<?php echo $plxShow->artId(); ?>">

							<header>
								<span class="art-date">
									<time datetime="<?php $plxShow->artDate('#num_year(4)-#num_month-#num_day'); ?>">
										<?php $plxShow->artDate('#num_day #month #num_year(4)'); ?>
									</time>
								</span>
								<h2>
									<?php $plxShow->artTitle('link'); ?>
								</h2>
								<div>
									<small>
										<span class="written-by">
											<?php $plxShow->lang('WRITTEN_BY'); ?> <?php $plxShow->artAuthor() ?>
										</span>
										<span class="art-nb-com">
											<?php $plxShow->artNbCom(); ?>
										</span>
									</small>
								</div>
								<div>
									<small>
										<!--<span class="classified-in">
											<?php $plxShow->lang('CLASSIFIED_IN') ?> : <?php $plxShow->artCat() ?>
										</span>
										<span class="tags">-->
											<?php $plxShow->lang('TAGS') ?> : <?php $plxShow->artTags() ?>
										</span>
									</small>
								</div>
							</header>

							<?php $plxShow->artThumbnail('<a href="#img_url"><img class="art_thumbnail" src="#img_thumb_url" alt="#img_alt" title="#img_title" /></a>', true, true); ?>
							<?php $plxShow->artChapo(); ?>

						</article>
					</div>
					<div class="col sml-hide med-hide lrg-1 lrg-show">&nbsp;</div>

				</div>
			<?php endwhile; } ?>

		</div>

		<nav class="pagination text-center">
			<?php $plxShow->pagination(); ?>
		</nav>

		<span>
			<?php $plxShow->artFeed('rss',$plxShow->catId()); ?>
		</span>

	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>
