<?php if (!defined('PLX_ROOT')) exit; ?>

	<footer>
		<div class="container">

			<div class="grid footer">
					<div class="audits col sml-12 med-12 lrg-12">
					        <p class="eco"> Site respectant l'environnement <a class="arrow-right" href="http://www.ecoindex.fr/resultatsv2/?id=77228">A sur Ecoindex</a> | <a href="https://gtmetrix.com/reports/mini-site.defis.info/aysyyzDn">A sur GTMetrix</a>.</p>
					        <p class="wheelchair"> Site accessible <a class="arrow-right" href="https://webaccessibility.com/">91% selon Level Access</a>.</p>
					</div>
				<div class="col sml-12 med-12 lrg-6 left">
					<p>
						<a href="core/admin/">&copy;</a> 2020 <?php $plxShow->mainTitle('link'); ?> - <?php $plxShow->subTitle(); ?>
						</br>
						<a href="index.php?static3/mentions-legales">Mentions L&eacute;gales</a>&nbsp;|
						<a href="#">Cr&eacute;dits photo</a>
					</p>
				</div>
				<div class="col sml-12 med-12 lrg-6 right licence">
					<p>
						<?php $plxShow->lang('POWERED_BY') ?>&nbsp;<a href="http://www.pluxml.org" title="<?php $plxShow->lang('PLUXML_DESCRIPTION') ?>">PluXml <?php echo $plxMotor->aConf['version'] ?></a>
						<?php $plxShow->lang('IN') ?>&nbsp;<?php $plxShow->chrono(); ?>
						</br>
						R&eacute;alis&eacute; par <a href="https://www.defis.info">Association D&eacute;fis</a>
						</br>
						<a href="#">Elegante Vitrine Thème</a> est disponible sous <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" src="<?php $plxShow->template(); ?>/img/CC-BY.png" /></a>
					</p>
				</div>
		</div>
	</footer>

</body>

</html>
